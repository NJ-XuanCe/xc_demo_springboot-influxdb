时区：末尾 tz('Asia/Shanghai')
SELECT * FROM "logInfo" WHERE time > now() - 5m tz('Asia/Shanghai')

SELECT * FROM "logInfo"  WHERE time >= '2024-12-16T03:38' AND time <= '2024-12-16T03:41'  ORDER BY time DESC LIMIT 10 OFFSET 0
## 批量写入
你可以通过以下修改来确保每个数据点都被正确写入：

确保每次循环时都调用 influxDB.write()

将 influxDB.write() 移到每次 batchPoints 填充后执行，而不是在外层循环结束时统一写入。
避免将 records 作为 BatchPoints 的 lineProtocol 来写入

直接使用 batchPoints 对象进行写入，而不是将 records 转换为 lineProtocol 格式。

## 时间戳唯一性
```java
Point point = Point.measurementByPOJO(logInfo.getClass())
.addFieldsFromPOJO(logInfo)
.time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
.build();
```
以上会正确插入1000条记录，如果把System.currentTimeMillis()改成一个固定值，只会有一条记录，是什么原因？

当你将 `System.currentTimeMillis()` 改为一个固定值时，只会插入一条记录的原因是，InfluxDB 使用 `time` 字段来区分不同的记录。如果所有记录的 `time` 字段值相同，InfluxDB 会认为它们是相同的时间戳，因此会覆盖之前的记录，而不是插入新的记录。

### 详细原因：
1. **`time` 字段唯一性**：InfluxDB 中的每条记录都需要一个唯一的 `time` 字段值。通常情况下，`time` 是时间戳，用来标识记录的时间。如果你使用固定的时间戳（例如 `2024-12-16T03:39:00`），所有记录的时间戳都相同，InfluxDB 会认为它们是相同的记录，并只保留最新的一个记录。

2. **时间戳冲突**：当你使用 `System.currentTimeMillis()` 时，时间戳是动态生成的，通常每次插入记录时时间戳会不同，因此每条记录都有不同的时间戳，InfluxDB 会根据时间戳区分每条记录并插入所有记录。而如果你使用固定时间戳，所有记录的时间戳相同，InfluxDB 就只会插入一条记录。

### 解决方法：
如果你想使用固定的时间戳，可以在每条记录中使用不同的时间戳，例如通过为每个记录增加一个递增的时间偏移量来确保每条记录的时间戳不同。

### 修改方案：
你可以通过在固定时间戳的基础上，增加一个小的偏移量（例如每次插入记录时增加几毫秒）来确保每条记录的时间戳唯一：

```java
long fixedTime = 1672531199000L;  // 假设这是你想使用的固定时间戳

for (int j = 0; j < 1000; j++) {
    long timeWithOffset = fixedTime + j;  // 为每条记录增加偏移量，确保时间戳唯一

    LogInfo logInfo = LogInfo.builder()
            .level("normal")
            .module("waterMeter")
            .deviceId(deviceId)
            .msg("" + j).build();

    Point point = Point.measurementByPOJO(logInfo.getClass())
            .addFieldsFromPOJO(logInfo)
            .time(timeWithOffset, TimeUnit.MILLISECONDS)  // 使用带偏移量的时间戳
            .build();

    batchPoints.point(point);
}
```

### 解释：
- `fixedTime + j`: 通过在固定时间戳的基础上增加递增的偏移量（如 `j`），确保每条记录的时间戳不同。
- 这样，InfluxDB 会为每条记录插入一个唯一的时间戳，从而插入 1000 条记录，而不是只插入一条。

### 总结：
使用固定时间戳时，需要确保每条记录的时间戳唯一，否则 InfluxDB 会认为它们是相同的记录并只插入一条。通过增加时间戳的偏移量，你可以确保每条记录的时间戳不同，从而插入多条记录。

## influxQL
time 字段默认是以 UTC 时区为准，因此建议使用 Z 后缀表示 UTC 时间（如 2024-12-16T03:38:00Z）。
InfluxDB 默认时间精度为纳秒（ns），如果你在查询时没有指定时间精度，它会自动使用纳秒单位。如果你使用的是其他时间精度（如秒、毫秒），需要在查询中进行相应的调整。

SELECT * FROM "logInfo" WHERE time >= '2024-12-16T03:38:00Z' AND time <= '2024-12-16T03:41:00Z'

# 时区
InfluxDB 默认存储时间为 **UTC（协调世界时）**，而不是本地时间。因此，在查询或写入数据时，需要考虑时区转换。

### 解决方法

#### 1. **在写入数据时处理时区**
确保写入数据的时间字段已经转换为 UTC 时间。如果你的数据是本地时间，可以通过代码将其转换为 UTC。

**JavaScript 示例：**
```javascript
const now = new Date();
const utcTime = now.toISOString(); // 转换为 UTC 格式的时间
```
然后在写入时使用 `utcTime`。

---

#### 2. **在查询数据时调整时区**
InfluxDB 的查询语言支持在查询时调整时区。例如，使用 `tz()` 指定查询时的时区：

**示例：**
```sql
SELECT * FROM "measurement_name" tz('Asia/Shanghai')
```
`Asia/Shanghai` 是中国标准时间 (CST)。

---

#### 3. **在 InfluxDB Studio 中设置时区**
InfluxDB Studio 默认显示 UTC 时间。如果需要显示为本地时间：
- 查看 InfluxDB Studio 的设置，确认是否支持调整显示时区的功能。
- 如果不支持，可以在查询中手动转换时间。

---

#### 4. **通过 InfluxQL 或 Flux 查询转换时区**
- 如果使用 InfluxQL，查询时可以通过客户端工具手动转换时间。
- 如果使用 Flux（InfluxDB 2.x），可以通过 `timeShift()` 函数调整时间：

**Flux 示例：**
```flux
from(bucket: "your_bucket")
  |> range(start: -1h)
  |> timeShift(duration: 8h) // 将 UTC 时间调整为 CST（+8 小时）
```

---

#### 5. **在 InfluxDB 配置中设置时区（不推荐）**
InfluxDB 的服务器级别通常不建议修改时区，因为它会影响所有数据的存储和查询行为。建议保持 UTC 时间存储，客户端处理时区转换。

---

### 推荐方案
- **写入时：** 确保时间为 UTC。
- **查询或显示时：** 根据需要转换为本地时间，例如 `Asia/Shanghai` (CST)。