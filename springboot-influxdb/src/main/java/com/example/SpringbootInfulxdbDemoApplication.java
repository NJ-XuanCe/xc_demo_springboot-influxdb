package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//https://www.cnblogs.com/jason1990/p/11076310.html
//https://github.com/influxdata/influxdb-java
@SpringBootApplication
public class SpringbootInfulxdbDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootInfulxdbDemoApplication.class, args);
    }

}
